//
//  PatternLayerCachItem.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 7/7/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  These items are used for caching and they store a single value
//  per warp/weft.  This is used by the PatternLayerCache.

import UIKit

struct PatternLayerCachItem {
    var color: UIColor?
    var threadDiameter: CGFloat?
}
