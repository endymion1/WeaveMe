//
//  PatternBuildError.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 8/8/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  These are the kinds of errors that can be returned by one of the Pattern's build methods
//  if something about the input data was incorrect.,
//
//  Eventually if we support loading from image URLs, etc.  We may need to expand this.

import Foundation

enum PatternBuildError: Int, Error, LocalizedError, CustomStringConvertible {
    
    case imageCouldNotBeLoaded = 120
    case nonMatchingImageDimensions = 121
    case invalidGroundLayerColor = 122
    case invalideShuttleLayerColor = 123
    case imageIsNotRGBA = 124
    case imageIsTooSmall = 125
    case noImagesProvided = 126
    case builderFailedWithNoError = 127
    
    var errorDescription: String? {
        switch self {
        case .imageCouldNotBeLoaded:
            return "The image could not be loaded."
        case .nonMatchingImageDimensions:
            return "The layers do not have matching dimensions."
        case .invalidGroundLayerColor:
            return "The ground layer does not have consistent warp or weft colors."
        case .invalideShuttleLayerColor:
            return "The shuttle layers do not have a consistent color."
        case .imageIsNotRGBA:
            return "The image was not RGBA format."
        case .imageIsTooSmall:
            return "The images used in patterns must be at least 2 by 2."
        case .noImagesProvided:
            return "No images were provided to build a pattern from."
        case .builderFailedWithNoError:
            return "Builder failed with no error."
        }
    }
    
    var description: String {
        return "\(errorDescription ?? "Unknown") (\(type(of: self)).\(self.rawValue))"
    }
    
}
