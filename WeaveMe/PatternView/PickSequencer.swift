//
//  PickSequencer.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/9/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  A helper class that helps step though all the picks in a pattern.  It
//  holds the code that keeps track of the current state, and what it's called.
//  This is used by the PatternViewController
//
//  Y's are processed from weftCount - 1 to 0  (bottom to top) and picks are processed
//  from first to last.

import UIKit

class PickSequencer {

    var pattern: Pattern? {
        didSet {
            guard let pattern = pattern,
            let weftCount = pattern.threadCount(.weft) else {
                currentYPicks = nil
                currentYIndex = nil
                currentYPickIndex = nil
                return
            }
            currentYIndex = weftCount - 1
            currentYPicks = pattern.weftPicksForY(weftCount - 1)
            currentYPickIndex = 0
        }
    }
    
    private(set) var currentYIndex: Int?
    private var currentYPicks: [WeftPick]?
    private var currentYPickIndex: Int?
    
    var currentWeftPick: WeftPick? {
        guard let picks = currentYPicks,
            let index = currentYPickIndex else {
                return nil
        }
        if index >= picks.count || index < 0 {
            return nil
        }
        return picks[index]
    }
    
    /// Use this to specify a current y, which will default to the first
    /// pick on that row.
    func setCurrentY(_ y: Int) {
        guard let pattern = pattern,
            let weftCount = pattern.threadCount(.weft) else {
            return
        }
        if y > weftCount - 1 || y < 0 {
            NSLog("Invalid value for setCurrentY \(y)")
            return
        }
        currentYIndex = y
        currentYPicks = pattern.weftPicksForY(y)
        currentYPickIndex = 0
    }
    
    /// The text used to describe our current position.
    var currentPositionName: String {
        guard let weftCount = pattern?.threadCount(.weft),
        let currentYIndex = currentYIndex,
        let currentYPicks = currentYPicks,
        let currentYPickIndex = currentYPickIndex else {
                return "-"
        }
        
        let rowNumber = weftCount - currentYIndex
        if currentYPicks.count > 1 {
            return "\(rowNumber)-\(currentYPickIndex + 1)"
        }
        return "\(rowNumber)"
    }
    
    /// This says if calling moveUp will do anything.
    var upShouldBeEnabled: Bool {
        guard let currentYIndex = currentYIndex,
            let currentYPicks = currentYPicks,
            let currentYPickIndex = currentYPickIndex else {
                return false
        }
        return currentYIndex > 0 || currentYPickIndex < currentYPicks.count - 1
    }
    
    /// Causes the sequencer to either move to the next pick, or the next y row.
    func moveUp() {
        guard let pattern = pattern,
            let currentYIndex = currentYIndex,
            let currentYPicks = currentYPicks,
            let currentYPickIndex = currentYPickIndex else {
                return
        }
        if currentYPickIndex < currentYPicks.count - 1 {
            self.currentYPickIndex = currentYPickIndex + 1
            return
        }
        if currentYIndex > 0 {
            self.currentYIndex = currentYIndex - 1
            self.currentYPicks = pattern.weftPicksForY(currentYIndex - 1)
            self.currentYPickIndex = 0
        }
    }
    
    /// This says if calling moveDown will do anything.
    var downShouldBeEnabled: Bool {
        guard let weftCount = pattern?.threadCount(.weft),
            let currentYIndex = currentYIndex,
            let currentYPickIndex = currentYPickIndex else {
                return false
        }
        return currentYIndex <  weftCount - 1 || currentYPickIndex > 0
    }
    
    /// Cause either to move back to the previous pick, or the last pick of the next row down.
    func moveDown() {
        guard let pattern = pattern,
            let weftCount = pattern.threadCount(.weft),
            let currentYIndex = currentYIndex,
            let currentYPickIndex = currentYPickIndex else {
                return
        }
        if currentYPickIndex > 0 {
            self.currentYPickIndex = currentYPickIndex - 1
            return
        }
        if currentYIndex < weftCount - 1 {
            self.currentYIndex = currentYIndex + 1
            self.currentYPicks = pattern.weftPicksForY(currentYIndex + 1)
            if let numberOfYPicks = self.currentYPicks?.count {
                self.currentYPickIndex = numberOfYPicks - 1
            } else {
                self.currentYPickIndex = nil
            }
        }
    }
    
}
