//
//  PatternThumbnailView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/12/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is just an ugly copy of the scroll view thing + a built in crop rectangle. Really we need to make
//  a better system for this.
//

import UIKit

class PatternThumbnailView: UIView {

    var pattern: Pattern?
    var cropRect: CGRect?
    
    private func renderAllPatternLayers(targetOutputWidth: CGFloat, completion: @escaping ([PatternLayer: UIImageView]?, Error?) -> Void) {
        guard let pattern = pattern else {
            completion(nil, nil)
            return
        }
        
        var errors: [Error] = []
        var layerToImage: [PatternLayer: UIImage] = [:]
        let dispatchGroup = DispatchGroup()
        
        for layer in pattern.patternLayers {
            // Right now we're using a different renderer for each layer. So we don't
            // have bad concurrent access issues.
            let renderer = PatternRenderer()
            guard let width = pattern.threadCount(.warp),
                let height = pattern.threadCount(.weft) else {
                    continue
            }
            dispatchGroup.enter()
            let style: PatternRenderer.RenderStyle = layer.layerType == .overlay ? .normalOnTopWeft : .normalWarpAndWeft
            renderer.renderPatternLayerRect(layer, style: style, rect: cropRect ?? CGRect.init(x: 0, y: 0, width: width, height: height), targetOutputWidth: targetOutputWidth) { image, error in
                if let image = image {
                    layerToImage[layer] = image
                } else {
                    if let error = error {
                        errors.append(error)
                    }
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            
            // If any of the layer renders failed we just report an error.
            if let anError = errors.first {
                completion(nil, anError)
                return
            }
            
            var layerToImageView: [PatternLayer: UIImageView] = [:]
            for layer in pattern.patternLayers {
                if let image = layerToImage[layer] {
                    let imageView = UIImageView.init(image: image)
                    imageView.contentMode = .scaleAspectFit
                    
                    var outputHeight: CGFloat = 0.0
                    if image.size.width != 0 && image.size.height != 0 {
                        let aspect: CGFloat = CGFloat(image.size.height)/CGFloat(image.size.width)
                        outputHeight = targetOutputWidth*aspect
                    }
                    
                    imageView.frame = CGRect.init(x: 0.0, y: 0.0, width: targetOutputWidth, height: outputHeight)
                    
                    layerToImageView[layer] = imageView
                }
            }
            completion(layerToImageView, nil)
        }
    }
    
    private var patternLayerToImageView: [PatternLayer: UIImageView] = [:]
    private var renderInProgress = false
    
    // This just adds the subviews in the same order as the layers since
    // they rendered out in some random order and we want them in a matching order.
    private func setupBasedOnLayerToImageView(_ layerToImageView: [PatternLayer: UIImageView]) {
        guard let layers = pattern?.patternLayers else {
            return
        }
        for layer in layers {
            if let imageView = layerToImageView[layer] {
                addSubview(imageView)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if patternLayerToImageView.isEmpty && !renderInProgress {
            renderInProgress = true
            renderAllPatternLayers(targetOutputWidth: bounds.size.width) { layerToImageView, error in
                self.renderInProgress = false
                if let layerToImageView = layerToImageView {
                    self.patternLayerToImageView = layerToImageView
                    self.setupBasedOnLayerToImageView(layerToImageView)
                }
            }
        }
    }
}
