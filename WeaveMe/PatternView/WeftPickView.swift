//
//  WeftPickView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/7/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is used to show just a subsection of weaving pattern in the
//  PatternScrollView.  This is mostly used to highlight individual picks in place
//  as the user is stepping though a weave.
//
//  If a warpSourceLayer is provided then the pick highlight will include
//  a dimmed version of the warp thread that pass over the highlighted weft thread
//  to make it look like it's rendering in place with the warp threads above it.

import UIKit

class WeftPickView: UIView {
    private(set) var weftPick: WeftPick?
    private(set) var warpSourceLayer: PatternLayer?
    
    /// Use the given renderer to pre-render the parts we're going to need to display.
    func renderPick(_ weftPick: WeftPick, warpSourceLayer: PatternLayer?, renderer: PatternRenderer,
                    completion: @escaping (_ successful: Bool, Error?) -> Void) {
        self.weftPick = weftPick
        self.warpSourceLayer = warpSourceLayer
        
        var errors: [Error] = []
        let dispatchGroup = DispatchGroup()
        
        let weftSettings = RenderSettings.init(rect: weftPick.rect, style: .normalWeft, outputImageWidth: bounds.size.width)
        
        dispatchGroup.enter()
        let weftLayerView = PatternLayerView.init(frame: self.bounds, patternLayer: weftPick.patternLayer, renderSettings: weftSettings)
        self.weftLayerView = weftLayerView
        weftLayerView.render(renderer: renderer) { success, error in
            if let error = error {
                errors.append(error)
            }
            if success {
                self.insertSubview(weftLayerView, at: 0)
            }
            dispatchGroup.leave()
        }
        
        if let warpLayer = warpSourceLayer {
            let warpSettings = RenderSettings.init(rect: weftPick.rect, style: .normalOnTopWarp, outputImageWidth: bounds.size.width)
            dispatchGroup.enter()
            let warpLayerView = PatternLayerView.init(frame: self.bounds, patternLayer: warpLayer, renderSettings: warpSettings)
            warpLayerView.alpha = 0.25
            self.warpLayerView = warpLayerView
            warpLayerView.render(renderer: renderer) { success, error in
                if let error = error {
                    errors.append(error)
                }
                if success {
                    self.addSubview(warpLayerView)
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completion(errors.isEmpty, errors.first)
        }
    }
    
    private var weftLayerView: PatternLayerView?
    private var warpLayerView: PatternLayerView?
}
