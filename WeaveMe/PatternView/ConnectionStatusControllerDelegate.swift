//
//  ConnectionStatusControllerDelegate.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/26/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//

import UIKit

protocol ConnectionStatusControllerDelegate: class {

    /// Called when the status controller would like to change text indicating the connection status.
    func connectionStatusController(_ connectionStatusController: ConnectionStatusController, didChangeStatusText titleText: String)

}
