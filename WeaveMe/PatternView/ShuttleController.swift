//
//  ShuttleController.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 9/15/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This just handles the animation of a WeftPickShuttleView in some other view.
//

import UIKit

class ShuttleController {
    
    var view: UIView?
    // TODO: Fix this so it actually computes this from instes or navigation bar or something.
    // since the current positioning is kind of stupid.
    var topPadding: CGFloat = 50.0
    var centerX: CGFloat = 0.0
    
    private var weftPickShuttleView: WeftPickShuttleView?
    func showShuttle(for weftPick: WeftPick, animated: Bool, completion: @escaping (Bool) -> Void) {
        guard let view = view else {
            return
        }
        
        if weftPickShuttleView == nil {
            weftPickShuttleView = WeftPickShuttleView.instantiate()
        }
        guard let shuttleView = weftPickShuttleView  else {
            return
        }
        shuttleView.alpha = 0.0
        shuttleView.weftPick = weftPick

        var padding = topPadding
        if #available(iOS 11.0, *) {
            padding += UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0
        }

        shuttleView.center = CGPoint(x: centerX, y: shuttleView.bounds.size.height/2.0 + padding)
        
        view.addSubview(shuttleView)
        
        if weftPick.direction == .leftToRight {
            shuttleView.transform = CGAffineTransform(translationX: -20.0, y: 0.0)
        } else {
            shuttleView.transform = CGAffineTransform(translationX: 20.0, y: 0.0)
        }
        
        UIView.animate(withDuration: animated ? 0.4 : 0.0, delay: 0.0, options: .curveEaseOut, animations: {
            shuttleView.alpha = 1.0
            shuttleView.transform = .identity
        }, completion: { (finished) in
            completion(finished)
        })
    }
    
    func hideShuttle(animated: Bool, completion: @escaping ((Bool) -> Void)) {
        guard let shuttleView = weftPickShuttleView else {
            completion(true)
            return
        }
        UIView.animate(withDuration: animated ? 0.1 : 0.0, animations: {
            shuttleView.alpha = 0
        }, completion: { (finished) in
            shuttleView.removeFromSuperview()
            completion(finished)
        })
    }
}
