//
//  LineGraphView.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 5/23/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
// Basic view for plotting a line graph of values. You provide the
// values in an array, valueRangeMax will be at the top of the view
// valueRangeMin will be at the bottom of the view, and the values will
// go from left to right with xStep between values.
//
// This is mostly just used to graph analog values being transmitted from
// the board.

import UIKit

class LineGraphView: UIView {
    
    var values: [CGFloat] = []
    
    var valueRangeMin: CGFloat = 0.0  // The value at the bottom of the view
    var valueRangeMax: CGFloat = 1.0  // The value at the top of the view
    var xStep: CGFloat = 1.0    // Step in points between values horizontally
    
    var lineWidth: CGFloat = 1.0
    var color = UIColor.black

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        var x: CGFloat = 0.0
        var y: CGFloat = 0.0
        
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(lineWidth)

        let rangeSize = valueRangeMax - valueRangeMin
        
        if rangeSize == 0 {
            return
        }
        
        if values.count > 0 {
            y = bounds.size.height - ((values[0] - valueRangeMin)/rangeSize)*bounds.size.height
            context.move(to: CGPoint(x: x, y: y))
        }

        var isFirst = true
        for value in values {
            if isFirst {
                isFirst = false
            } else {
                y = bounds.size.height - ((value - valueRangeMin)/rangeSize)*bounds.size.height
                context.addLine(to: CGPoint.init(x: x, y: y))
            }
            x += xStep
        }
        context.strokePath()
    }
}
