//
//  LimitIndicatorTracker.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 11/11/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This just fades in/out an indicators based on commands that come in from
//  the device over bluetooth.  We may eventually support a warning state as
//  well so I broke this into its own class.

import UIKit

class LimitIndicatorTracker: NSObject {

    var isActive = true {
        didSet {
            if isActive {
                if !isRegistered {
                    addListeners()
                }
            } else {
                if isRegistered {
                    removeListeners()
                }
                UIView.animate(withDuration: 0.1, delay: 0.0, options: .beginFromCurrentState, animations: {
                    self.indicatorImageView.alpha = 0.0
                })
            }
        }
    }

    private var prefix: String
    private var indicatorImageView: UIImageView
    private var isRegistered = false
    private var limitName: String

    init(limitName: String, prefix: String, indicatorImageView: UIImageView) {
        self.limitName = limitName
        self.prefix = prefix
        self.indicatorImageView = indicatorImageView
        super.init()
        // In the storyboard it's nice to hide these instead of having 0 alpha
        // so you can still see a ghosted version.  So we unhide on startup.
        indicatorImageView.alpha = 0.0
        indicatorImageView.isHidden = false
        addListeners()
    }

    private func addListeners() {
        KSBluetoothManager.shared.addCommandListener(listener: self, for: prefix + "0", withDescription: limitName + " Limit Switch Inactive")
        KSBluetoothManager.shared.addCommandListener(listener: self, for: prefix + "1", withDescription: limitName + " Limit Switch Active")
        isRegistered = true
    }

    private func removeListeners() {
        KSBluetoothManager.shared.removeCommandListener(listener: self, for: prefix + "0")
        KSBluetoothManager.shared.removeCommandListener(listener: self, for: prefix + "1")
        isRegistered = false
    }
}

extension LimitIndicatorTracker: KSBluetoothManagerCommandListener {

    func didReceiveCommand(_ command: String, argument: String) {
        if command == prefix + "0" {
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .beginFromCurrentState, animations: {
                self.indicatorImageView.alpha = 0.0
            })
        }
        if command == prefix + "1" {
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .beginFromCurrentState, animations: {
                self.indicatorImageView.alpha = 1.0
            })
        }
    }

}
