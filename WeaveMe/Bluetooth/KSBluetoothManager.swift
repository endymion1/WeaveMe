//
//  KSBluetoothManager.swift
//  WeaveMe
//
//  Created by  Kurt Schaefer on 5/10/18.
//  Copyright © 2018 RetroTechJurnal. All rights reserved.
//
//  This is a singleton wrapper around a KSBluetoothPeripheral.  Direct pass throughs for various connection
//  management calls are provided, and users can register as listeners to find out about connection state, and
//  to get specific commands.  These commands should be very short since 9600 baud is slow.
//

import UIKit

class KSBluetoothManager: NSObject, KSBluetoothPeripheralDelegate {
    static let shared = KSBluetoothManager()
    
    override private init() {
        super.init()
        peripheral.delegate = self
    }
  
    private let stateChangeListeners: MulticastDelegate<KSBluetoothManagerStateListener> = MulticastDelegate<KSBluetoothManagerStateListener>.init()
    private var commandToListeners: [String: MulticastDelegate<KSBluetoothManagerCommandListener>] = [:]
    private var commandToDescription: [String: String] = [:]
    private var notifyListenersInProgress = false

    private let peripheral = KSBluetoothPeripheral()
    var connectionState: KSPeripheralConnectionState {
        return peripheral.connectionState
    }
    
    func addStateChangeListener(listener: KSBluetoothManagerStateListener) {
        stateChangeListeners.add(delegate: listener)
    }
    
    func scanForPeripherals() {
        peripheral.scanForPeripherals()
    }
    
    func disconnect() {
        peripheral.disconnect()
    }
    
    let notConnectedResponse = "Not Connected"
    func sendCommand(_ command: String, annotation: String, success: @escaping ((String?) -> Void), failure: @escaping ((String?) -> Void), finally: @escaping () -> Void) {
        peripheral.send(command, success: success, failure: failure, finally: finally)
    }
    
    func writeRawData(_ data: Data) {
        peripheral.writeRawData(data)
    }
    
    func addCommandListener(listener: KSBluetoothManagerCommandListener, for command: String, withDescription description: String) {
        let listeners = commandToListeners[command] ?? MulticastDelegate<KSBluetoothManagerCommandListener>.init()
        listeners.add(delegate: listener)
        commandToListeners[command] = listeners
        
        if let existingDescription = commandToDescription[command] {
            assert(existingDescription == description,
                   "KSBlueToothManager: \(command) being used for both \(description) and \(existingDescription)")
        }
        commandToDescription[command] = description
    }
    
    func removeStateChangeListener(listener: KSBluetoothManagerStateListener) {
        assert(!notifyListenersInProgress, "KSBluetoothManager: We do not currently support removeListener during a notify.")
        stateChangeListeners.remove(delegate: listener)
    }
    
    func removeCommandListener(listener: KSBluetoothManagerCommandListener, for command: String) {
        assert(!notifyListenersInProgress, "KSBluetoothManager: We do not currently support removeListener during a notify.")
        if let listeners = commandToListeners[command] {
            listeners.remove(delegate: listener)
            commandToListeners[command] = listeners
        }
    }
 
    private func notifyConnectionStateListeners(newConnectionState: KSPeripheralConnectionState) {
        assert(!notifyListenersInProgress, "KSBluetoothManager: We do not currently support nested listener notifications.")
        notifyListenersInProgress = true
        stateChangeListeners.invoke() { listener in
            listener.connectionStateChanged(newConnectionState)
        }
        notifyListenersInProgress = false
    }
    
    private func notifyListenersOfCommand(_ command: String, argument: String) {
        assert(!notifyListenersInProgress, "KSBluetoothManager: We do not currently support nested listener notifications.")
        
        guard let listeners = commandToListeners[command]  else {
            NSLog("No one waiting for command %@", command)
            return
        }
        notifyListenersInProgress = true
        listeners.invoke() { listener in
            listener.didReceiveCommand(command, argument: argument)
        }
        notifyListenersInProgress = false
        
    }
    
    // MARK: - KSBluetoothPeripheralDelegate
    func didReceive(_ string: String!) {
        // Many commands have no args and they're just L0 or something. In those cases
        // the argument is ""
        var command = string
        var argument = ""
        let partArray = string.components(separatedBy: " ")
        if let firstPart = partArray.first, partArray.count > 1 {
            command = firstPart
            argument = String(string)
            
            // If it does have args we strip off the prefix to keep every listener from having to do it.
            let index = argument.index(argument.startIndex, offsetBy: firstPart.count + 1)
            argument = String(argument[index...])
        }
        // TODO: Port the bluetooth thing to swift so we can be done with this implicit optionals horror, etc.
        guard let notificationCommand = command else {
            return
        }
        notifyListenersOfCommand(notificationCommand, argument: argument)
    }

    func connectionStateChanged(_ connectionState: KSPeripheralConnectionState) {
        notifyConnectionStateListeners(newConnectionState: connectionState)
    }

}
