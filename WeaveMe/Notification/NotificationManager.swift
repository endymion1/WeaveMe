//
//  NotificationManager.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 5/11/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This just keeps all the notifications/names in one place to keep them better documented.

import UIKit

extension Notification.Name {

    static let warpStateDidUpdate = Notification.Name("warpStateDidUpdate")

}

class NotificationManager: NSObject {

    static let shared = NotificationManager()
    override private init() {}

    // This lets those that display the warp state know that it has changed.
    func warpStateDidUpdate() {
        assert(Thread.isMainThread)
        NotificationCenter.default.post(name: Notification.Name.warpStateDidUpdate, object: nil)
    }

}
