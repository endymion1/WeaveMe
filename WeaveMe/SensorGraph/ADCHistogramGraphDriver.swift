//
//  ADCHistogramGraphDriver.swift
//  WeaveMe
//
//  Created by Kurt Schaefer on 4/20/19.
//  Copyright © 2019 RetroTechJurnal. All rights reserved.
//
//  This takes a series of readings from a 10 bit ADC and makes a histogram of the results.
//  This kind of view can be useful to determine the amount of electrical/mechanical noise in the system.

import UIKit

class ADCHistogramGraphDriver: NSObject {
    private let barGraphView: BarGraphView
    private var command: String
    private var annotation: String

    let bucketCount = 256
    private(set) var buckets: [Int]

    private var minValue = 0
    private var maxValue = 1023

    init(barGraphView: BarGraphView, command: String, annotation: String) {
        self.barGraphView = barGraphView
        self.command = command
        self.annotation = annotation
        // Although ADC output is 10 bit we use fewer buckets so they are bigger.
        self.buckets = Array(repeating: 0, count: bucketCount)
    }

    /// request the given number of samples and then push the resulting histogram
    /// into the barGraphView.
    func update(with sampleCount: Int, completion: @escaping () -> Void) {
        resetBuckets()
        requestSamples(count: sampleCount) {
            self.displayBuckets()
            completion()
        }
    }

    func populateWithFakeDataAndDisplay() {
        for index in buckets.indices {
            buckets[index] = Int.random(in: 0..<1023)
        }
        displayBuckets()
    }

    private func bucketIndex(for value: Int) -> Int {
        assert(value >= minValue || value <= maxValue)
        let rangeSize = maxValue - minValue
        let maxBucketIndex = Float(buckets.count - 1)
        return Int(floor(Float(value - minValue)/Float(rangeSize)*maxBucketIndex))
    }

    private func ingestValue(_ value: Int?) {
        guard let value = value else {
            return
        }

        let index = bucketIndex(for: value)
        NSLog("got value \(value) index \(index)")
        buckets[bucketIndex(for: value)] += 1
    }

    private func requestValue(command: String, completion: @escaping (Int?) -> Void) {
        KSBluetoothManager.shared.sendCommand(command, annotation: annotation,
                                              success: { response in
                                                if let responseString = response {
                                                    completion(Int(responseString))
                                                } else {
                                                    completion(nil)
                                                }
        }, failure: { response in
            completion(nil)
        }, finally: {
        })
    }

    private func requestSamples(count: Int, completion: @escaping () -> Void) {
        if count < 1 {
            completion()
            return
        }

        // These BT requests can't be done in paralel so we use a serial queue to
        // make them happen serially, and an ordering queue to give them a way to
        // do their own async requests in order.
        let queue = DispatchQueue(label: "sampleCommandQueue")
        let orderingGroup = DispatchGroup()
        let requestGroup = DispatchGroup()

        for _ in 1...count {

            requestGroup.enter()
            queue.async {
                // Everyone waits on the ordering group so we make these requests sequentially
                orderingGroup.wait()
                orderingGroup.enter()

                self.requestValue(command: self.command, completion: { value in
                    self.ingestValue(value)
                    orderingGroup.leave()
                    requestGroup.leave()
                })
            }
        }

        // This notify happens afer we have finally gotten all the requests.
        requestGroup.notify(queue: .main) {
            completion()
        }
    }

    public func resetBuckets() {
        for index in buckets.indices {
            buckets[index] = 0
        }
    }

    private func getBucketRange() -> (min: Int, max: Int) {
        var min = buckets[0]
        var max = buckets[0]

        for bucket in buckets {
            if bucket > max {
                max = bucket
            }
            if bucket < min {
                min = bucket
            }
        }
        return (min: min, max: max)
    }

    public func displayBuckets() {
        let bucketRange = getBucketRange()
        barGraphView.valueRangeMin = CGFloat(bucketRange.min)
        barGraphView.valueRangeMax = CGFloat(bucketRange.max)

        var newValues = Array(repeating: CGFloat(0), count: buckets.count)
        for (index, bucket) in buckets.enumerated() {
            newValues[index] = CGFloat(bucket)
        }

        barGraphView.values = newValues
        barGraphView.setNeedsDisplay()
    }

}
